import java.util.Scanner;

public class Agenda {
    final String LUNI = "luni";
    final String MARTI = "marti";
    final String MIERCURI = "miercuri";
    final String JOI = "joi";
    final String VINERI = "vineri";
    final String SAMBATA = "sambata";
    final String DUMINICA = "duminica";

    public void tellMeWhatToDo(String day) {

        if (day.equals(LUNI)) {
            System.out.println("Astazi este Luni, munca");
        } else if (day.equals(MARTI)) {
            System.out.println("Azi este Marti, munca");
        } else if (day.equals(MIERCURI)) {
            System.out.println("Azi este Miercuri, munca");
        } else if (day.equals(JOI)) {
            System.out.println("Azi este Joi, munca");
        } else if (day.equals(VINERI)) {
            System.out.println("Azi este Vineri, munca, maine este weekend");
        } else if (day.equals(SAMBATA)) {
            System.out.println("Azi este Sambata, party");
        } else if (day.equals(DUMINICA)) {
            System.out.println("Azi este Duminica, zi de odihna");
        }
    }

    public void tellMeWhatToDoSwitch(String day) {    //semnatura metodei
        switch (day) {   // implementarea metodei
            case LUNI:
                System.out.println("Astazi este Luni, munca");
                break;
            case MARTI:
                System.out.println("Azi este Marti, munca");
                break;
            case MIERCURI:
                System.out.println("Azi este Miercuri, munca");
                break;
            case JOI:
                System.out.println("Azi este Joi, munca");
                break;
            case VINERI:
                System.out.println("Azi este Vineri, munca, maine este weekend");
                break;
            case SAMBATA:
                System.out.println("Azi este Sambata, party");
                break;
            case DUMINICA:
                System.out.println("Azi este Duminica, zi de odihna");
                break;
            default:
                System.out.println("Alege o zi");
        }
    }

         static int sumatorDe2Numere(int param1, int param2) {

          int sum = (param1 + param2);
          return sum; //sumator de doua numere
        }
}
